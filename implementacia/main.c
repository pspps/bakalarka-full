#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define obrazokX 500
#define obrazokY 500

const unsigned int pocet_rozmerov = 2;
const double delta[] = {1.l, 1.l};
const double deltaT = 1.l;
const unsigned int rozmer_obrazku[] = {obrazokX, obrazokY};
const unsigned int pocet_iteracii = 10;

double objemV;
double *poverchSteny;

//TODO: 2D
double **obrazok;
double **aktualne_riesenie;

double wcon = 1.l,
       wdif = 1.l,
       epsilon = .01l,
       Ka = 8.l;


#include "pos.h"
#include "matica.h"

double *vrat_iti_obrazka(POS pozicia){
	return vrat_iti_element(obrazok, pozicia);
}

double *vrat_iti_riesenia(POS pozicia){
	return vrat_iti_element(aktualne_riesenie, pozicia);
}

void init(){
	inicializuj_obrazok();
	inicializuj_riesenie();

	int i, j;

	objemV = 1.l;
	poverchSteny = (double *)malloc(sizeof(double)*pocet_rozmerov);

	for (i = 0; i < pocet_rozmerov; ++i)
	    poverchSteny[i] = 1.l;

	for (i = 0; i < pocet_rozmerov; ++i)
	{
	    objemV *= delta[i];

	    for (j = 0; j < pocet_rozmerov; ++j)
	    {
	        if(i!=j)
	        	poverchSteny[j] *= delta[i];
	    }
	}
	
}

double f(double v){
	return v;
}

double g(POS pozicia){
	double gradient_image = *vrat_iti_obrazka(pozicia);
	return f(1/(1 + Ka * gradient_image * gradient_image));
}

double koeficient_i(POS pozicia){
	if(pos_je_na_kraji(&pozicia))
		return 4*M_PI/M_E;

	double ret = 0.l;

	double casovy_koeficient = 0.l,
	       konveksny_koeficient = 0.l,
	       difuzny_koeficient = 0.l;

	casovy_koeficient = objemV/deltaT;

	int bazovy_vektor, hore;
	POS i;
	pos_init(&i, 0.l);
	pos_copy(&i, &pozicia);

	for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor)
	{
	    for (hore = 0; hore < 2; ++hore)
	    {
	    	pos_copy(&i, &pozicia);
	    	i[bazovy_vektor] += (hore ? 1 : -1);

	        konveksny_koeficient += (poverchSteny[bazovy_vektor]/delta[bazovy_vektor]) * (g(i) - g(pozicia));
		difuzny_koeficient += poverchSteny[bazovy_vektor]/(delta[bazovy_vektor] * sqrt(epsilon*epsilon + velkost_gradientu_na_hrane_na_druhu(aktualne_riesenie, i, bazovy_vektor, hore)));

		i[bazovy_vektor] = pozicia[bazovy_vektor];
	    }
	}
	pos_clean(&i);

	konveksny_koeficient *= wcon;
	difuzny_koeficient *= wdif * g(pozicia) * sqrt(epsilon*epsilon + velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia));


	ret = casovy_koeficient + konveksny_koeficient + difuzny_koeficient;
	return ret;
}

double koeficient_iplus(POS pozicia, int bazovy_vektor, int hore){
	if(pos_je_na_kraji(&pozicia))
		return 0.l;

	double ret = 0.l;

	double konveksny_koeficient = 0.l,
	       difuzny_koeficient = 0.l;

	double tmp_gradient;

	POS i;
	pos_init(&i, 0.l);
	pos_copy(&i, &pozicia);
	i[bazovy_vektor] += (hore ? 1 : -1);

	konveksny_koeficient = wcon * poverchSteny[bazovy_vektor] * (g(i) - g(pozicia)) / delta[bazovy_vektor];

	tmp_gradient = velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia);
	difuzny_koeficient = -wdif * g(pozicia) * sqrt(epsilon*epsilon + tmp_gradient) * poverchSteny[bazovy_vektor] / (delta[bazovy_vektor] * sqrt(epsilon*epsilon + velkost_gradientu_na_hrane_na_druhu (aktualne_riesenie, pozicia, bazovy_vektor, hore)));

	pos_clean(&i);

	ret = konveksny_koeficient + difuzny_koeficient;

	return ret;
}

double prava_strana(POS pozicia){
	if(pos_je_na_kraji(&pozicia))
		return 0.l;
	return (objemV/deltaT) * (*vrat_iti_riesenia(pozicia));
}

int main(int argc, char const *argv[])
{
	unsigned int cislo_riesenia;
	char *nazov_suboru;
	double **matica, *prava_strana;
	int i, j, k;
	POS i_pos, tmp_pos;
	unsigned int pocet_bodov = 1;

	printf("Ahoj svet\n");
	init();

	nazov_suboru = (char *)malloc(strlen("data/riesenie.00000.matica")*sizeof(char));

	exportuj_maticu(obrazok, "data/obrazok.matica");

	cyklus_cez_rozmery
		pocet_bodov *= rozmer_obrazku[i];
	pos_init_viacero(2, 0.l, &i_pos, &tmp_pos);

	matica = (double **)malloc(sizeof(double *) * pocet_bodov);
	prava_strana = (double *)malloc(sizeof(double) * pocet_bodov);

	for(i = 0; i<pocet_bodov; ++i)
		matica[i] = (double *)malloc(sizeof(double) * pocet_bodov);

	for(cislo_riesenia = 1; cislo_riesenia <= pocet_iteracii; ++cislo_riesenia){
		for(i = 0; i<pocet_bodov; ++i){
			index_to_pos(&i_pos, i);

			if(pos_je_na_kraji(&i_pos)){
				matica[i][i] = 1.l;
				//TODO
//				prava_strana[i] = 0.l;
				continue;
			}

			matica[i][i] = koeficient_i(i_pos);
			pos_copy(&i_pos, &tmp_pos);

			for(j = 0; j < pocet_rozmerov; ++j){
				matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 1);
				matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 0);
			}
		}

		sprintf(nazov_suboru, "data/riesenie.%d.matica", cislo_riesenia);
		exportuj_maticu(aktualne_riesenie, nazov_suboru);
	}

	return 0;
}
