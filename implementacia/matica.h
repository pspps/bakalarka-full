#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "pos.h"

#ifndef MATICADOTHA
#define MATICADOTHA
#define alokuj_maticu(v) alokuj_maticu_implementacia(v, 0)

double *vrat_iti_element(double **pole, POS pozicia){
	int i;
	double **pointer = pole;

	for(i = 0; i<pocet_rozmerov; ++i)
		if(pozicia[i] < 0 || pozicia[i] >= rozmer_obrazku[i])
			return 0;

	for (i = 0; i < pocet_rozmerov-1; ++i)
	{
	    pointer = (double **)pointer[pozicia[i]];
	}

	pointer = (double **) ((double *)pointer + pozicia[pocet_rozmerov-1]);

	return (double *)pointer;
}

void inicializuj_obrazok(){
	//TODO: 2D
	int i, j;
	const int sirka = 20;

	obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));

	
	for(int i = 0; i<rozmer_obrazku[0]; ++i){
		obrazok[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

		for (j = 0; j < rozmer_obrazku[1]; ++j){
			int x = i-(rozmer_obrazku[0]/2);
			int y = j-(rozmer_obrazku[1]/2);
			
			if(abs(x*x + y*y - 200*200) < sirka*sirka)
				obrazok[i][j] = 10.l;
		}
	}
}

void inicializuj_riesenie(){
	int i, j;

	aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


	for(int i = 0; i<rozmer_obrazku[0]; ++i){
		aktualne_riesenie[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

		for (j = 0; j < rozmer_obrazku[1]; ++j){
			int x = i-(rozmer_obrazku[0]/2);
			int y = j-(rozmer_obrazku[1]/2);
			
		    	if(abs(x*x + y*y < 50*50))
		    		aktualne_riesenie[i][j] = 10.l;
		}
	}
}

double *gradient_na_hrane(double **pole, POS pozicia, int bazovy_vektor, int hore){
	double *ret = (double *)malloc(sizeof(double)*pocet_rozmerov);
	int i;

	for(i=0; i<pocet_rozmerov; ++i)
		ret[i] = 0.l;

	POS pozicia_nove,
	    pozicia_horne,
            pozicia_dolne;
	pos_init_viacero(3, 0.l, &pozicia_nove, &pozicia_dolne, &pozicia_horne);

	for(i=0; i<pocet_rozmerov; i++){
		if(i == bazovy_vektor){
			pos_copy(&pozicia_nove, &pozicia);
			pozicia_nove[i] += ( hore ? 1 : -1);

			ret[i] = (vrat_iti_element(pole, pozicia_nove) - vrat_iti_element(pole, pozicia)) / delta[i];
			continue;
		}
		pos_copy(&pozicia_horne, &pozicia);
		pos_copy(&pozicia_dolne, &pozicia);

		pozicia_dolne[i] += -1;
		pozicia_horne[i] += 1;

		ret[i] += vrat_iti_element(pole, pozicia_horne) - vrat_iti_element(pole, pozicia_dolne);

		pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
		pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);

		ret[i] += vrat_iti_element(pole, pozicia_horne) - vrat_iti_element(pole, pozicia_dolne);

		ret[i] /= 4*delta[i];
	}

	pos_clean_viacero(3, &pozicia_nove, &pozicia_dolne, &pozicia_horne);

	return ret;
}

double velkost_gradientu_na_hrane_na_druhu(double **pole, POS pozicia, int bazovy_vektor, int hore){
	double *tmp, 
	       ret = 0.l;
	int i;

	tmp = gradient_na_hrane(pole, pozicia, bazovy_vektor, hore);
	for(i=0; i<pocet_rozmerov; ++i)
		ret += tmp[i]*tmp[i];

	free(tmp);

	return ret;
}

double *gradient_na_elemente(double **pole, POS pozicia){
	double *ret = (double *)malloc(sizeof(double)*pocet_rozmerov),
	       *tmp_double;
	int i, j;

	for(i=0; i<pocet_rozmerov; ++i)
		ret[i] = 0.l;

	for(i=0; i<2*pocet_rozmerov; ++i){
		tmp_double = gradient_na_hrane(pole, pozicia, i>>1, i&1);

		for(j=0; j<pocet_rozmerov; ++j)
			ret[j] += tmp_double[j];

		free(tmp_double);
	}

	for(j=0; j<pocet_rozmerov; ++j)
		ret[i] /= 2*pocet_rozmerov;

	return ret;
}

double velkost_gradientu_na_elemente_na_druhu(double **pole, POS pozicia){
	double *tmp, 
	       ret = 0.l;
	int i;

	tmp = gradient_na_elemente(pole, pozicia);
	for(i=0; i<pocet_rozmerov; ++i)
		ret += tmp[i]*tmp[i];

	free(tmp);

	return ret;
}

void exportuj_maticu(double **pole, char *nazov_suboru){
	//TODO 2D
	FILE *f = fopen(nazov_suboru, "w");
	int i, j;
	for(i = 0; i < rozmer_obrazku[0]; ++i){
		for(j = 0; j < rozmer_obrazku[1]; ++j){
			fprintf(f, "%lf ", pole[i][j]);
		}
		putc('\n', f);
	}
}

void alokuj_maticu_implementacia(double **v, int hlbka){
	int i;
	
	if(hlbka = pocet_rozmerov-1){
		*v = (double *)malloc(sizeof(double) * rozmer_obrazku[pocet_rozmerov-1]);
		
		for(i = 0; i<rozmer_obrazku[hlbka]; ++i)
			(*v)[i] = 0.l;

		return;
	}
	
	*v = (double *)malloc(sizeof(double *) * rozmer_obrazku[hlbka]);

	for(i = 0; i<rozmer_obrazku[hlbka]; ++i){
		alokuj_maticu_implementacia(&v[i], hlbka+1);
	}
}

#endif
