#ifndef EXTERN_H
#define EXTERN_H

extern const unsigned int pocet_rozmerov;
extern const double delta[];
extern const double deltaT;
extern const unsigned int rozmer_obrazku[];
extern const unsigned int pocet_iteracii;

extern double objemV;
extern double *poverchSteny;

//TODO: 2D
extern double **obrazok;
extern double **aktualne_riesenie;

extern double wcon,
       wdif,
       epsilon,
       Ka;

extern int pocet_bodov;

extern int n;
extern double h;

#endif // EXTERN_H

