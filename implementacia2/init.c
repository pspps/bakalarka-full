#include "init.h"


void inicializuj_obrazok(){
    //TODO: 2D
    int i, j;
    const int sirka = 4;
    const int polomer = 16;
//	inicializuj_obrazok_stvorec();
//	return;

    obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


    for(int i = 0; i<rozmer_obrazku[0]; ++i){
        obrazok[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[1]; ++j){
            int x = i-(rozmer_obrazku[0]/2);
            int y = j-(rozmer_obrazku[1]/2);

//            if(abs(x*x*2 + y*y - polomer*polomer) < sirka*sirka)
//            if(abs(x*x + y*y - polomer*polomer) < sirka*sirka && (x < 0 || abs(y) >4))
            if(x*x + y*y < polomer*polomer)
                obrazok[i][j] = 10.l;
			else
                obrazok[i][j] = 0.l;
        }
    }
//	vedenie_tepla(obrazok, .1l);
}

void inicializuj_obrazok_stvorec(){
    //TODO: 2D
    int i, j;

    obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


    for(int i = 0; i<rozmer_obrazku[0]; ++i){
        obrazok[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[1]; ++j){

//            if(abs(x*x*2 + y*y - polomer*polomer) < sirka*sirka)
            if(
					((abs(i-10) <= 1.1)||(abs(rozmer_obrazku[0]-i-10) <= 1.1))&& ((j>=10) && (j<=rozmer_obrazku[1]-10)) ||
					((abs(j-10) <= 1.1)||(abs(rozmer_obrazku[1]-j-10) <= 1.1))&& ((i>=10) && (i<=rozmer_obrazku[0]-10))
				)
                obrazok[i][j] = 10.l;
			else
                obrazok[i][j] = 0.l;
        }
    }
	//vedenie_tepla(obrazok, .05l);
}

void inicializuj_riesenie(){
    int i, j;
	const int polomer = 8;
//	const int polomer = 10;
	double tmp;

    aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


    for(int i = 0; i<rozmer_obrazku[0]; ++i){
        aktualne_riesenie[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[1]; ++j){
            int x = i-(rozmer_obrazku[0]/2);
            int y = j-(rozmer_obrazku[1]/2);
//            double x = i*delta[0];
//			double y = j*delta[1];

                if(abs(x*x + y*y < polomer*polomer)){
//                    aktualne_riesenie[i][j] = 100.l;
                    aktualne_riesenie[i][j] = (1.l /(1.l+ sqrt(x*x+y*y)) - 1.l/(1.l+polomer))*80;
				}
				else{
					aktualne_riesenie[i][j] = 0.l;
				}
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.4l*.4l);
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.2l*.2l);
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.1l*.1l);
//				if(tmp > 0.l){
//					aktualne_riesenie[i][j] = 0.l;
//				printf("---------%f %f, %f\n", x, y, tmp);
//				}
//				else{
//					aktualne_riesenie[i][j] = tmp*tmp*100;
//					aktualne_riesenie[i][j] = 10;
//					aktualne_riesenie[i][j] = tmp/2.l;
//				printf("~~~~~~~~~%f %f, %f\n", x, y, tmp);
//				}
        }
    }
//	vedenie_tepla(aktualne_riesenie, 20);
}


void init(){
    inicializuj_obrazok();
    inicializuj_riesenie();

    int i, j;

    objemV = 1.l;
    poverchSteny = (double *)malloc(sizeof(double)*pocet_rozmerov);

    for (i = 0; i < pocet_rozmerov; ++i)
        poverchSteny[i] = 1.l;

    for (i = 0; i < pocet_rozmerov; ++i)
    {
        objemV *= delta[i];

        for (j = 0; j < pocet_rozmerov; ++j)
        {
            if(i!=j)
                poverchSteny[j] *= delta[i];
        }
    }

}
void inicializuj_obrazok3d(){
    int i, j, k;
    const int sirka = 5;
    const int polomer = 15;

    obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double **));

for(k = 0; k <rozmer_obrazku[0]; ++k){ 
	obrazok[k] = (double *)malloc(rozmer_obrazku[1] * sizeof(double *));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
        ((double ***)obrazok)[k][i] = (double *)malloc(rozmer_obrazku[2] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[2]; ++j){
            int x = k-(rozmer_obrazku[0]/2);
            int y = i-(rozmer_obrazku[1]/2);
			int z = j-(rozmer_obrazku[2]/2);

//            if(abs(x*x*2 + y*y - polomer*polomer) < sirka*sirka)
            if(abs(x*x + y*y + z*z - polomer*polomer) < sirka*sirka)
                ((double ***)obrazok)[k][i][j] = 10.l;
        }
    }
}
//	vedenie_tepla(obrazok, 20);
}

void inicializuj_riesenie3d(){
    int i, j, k;
	const int polomer = 15;
	double tmp;

    aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));

for(k = 0; k<rozmer_obrazku[0]; ++k){
	aktualne_riesenie[k] = (double *)malloc(rozmer_obrazku[1] * sizeof(double *));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
        ((double ***)aktualne_riesenie)[k][i] = (double *)malloc(rozmer_obrazku[2] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[2]; ++j){
            int x = k-(rozmer_obrazku[0]/2);
            int y = i-(rozmer_obrazku[1]/2);
            int z = j-(rozmer_obrazku[2]/2);
//            double x = k*delta[0];
//			double y = i*delta[1];
//			double z = j*delta[2];

//            double x = k*delta[0] - .5l;
//			double y = i*delta[1] - .5l;
//			double z = j*delta[2] - .5l;
//
//
//
              if(abs(x*x + y*y + z*z < polomer*polomer)){
                    //aktualne_riesenie[i][j] = 100.l;
                    ((double ***)aktualne_riesenie)[k][i][j] = (1.l /(1.l+ sqrt(x*x+y*y+z*z)) - 1.l/(1.l+polomer))*20;
				}
				else{
					((double ***)aktualne_riesenie)[k][i][j] = 0.l;
				}
//
//
//
//
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l) + (z-.5l)*(z-.5l)-.4l*.4l);
//				tmp = (x*x + y*y + z*z-.4l*.4l);
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.2l*.2l);
//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.1l*.1l);
//				if(tmp > 0.l)
//					((double ***)aktualne_riesenie)[k][i][j] = 0.l;
//				else
//					aktualne_riesenie[i][j] = tmp*tmp*100;
//					aktualne_riesenie[i][j] = 10;
//					((double ***)aktualne_riesenie)[k][i][j] = tmp/4.l;
        }
    }
}
//	vedenie_tepla(aktualne_riesenie, 20);
}


void init3d(){
    inicializuj_obrazok3d();
    inicializuj_riesenie3d();

    int i, j;

    objemV = 1.l;
    poverchSteny = (double *)malloc(sizeof(double)*pocet_rozmerov);

    for (i = 0; i < pocet_rozmerov; ++i)
        poverchSteny[i] = 1.l;

    for (i = 0; i < pocet_rozmerov; ++i)
    {
        objemV *= delta[i];

        for (j = 0; j < pocet_rozmerov; ++j)
        {
            if(i!=j)
                poverchSteny[j] *= delta[i];
        }
    }

}

void inicializuj_obrazok4d(){
    int i, j, k, l;
    const int sirka = 5;
    const int polomer = 15;
	inicializuj_obrazok4d_translacia();
	return;

	obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double **));
	double ****ptr = (double ****)obrazok;

for(k = 0; k <rozmer_obrazku[0]; ++k){ 
	ptr[k] = (double ***)malloc(rozmer_obrazku[1] * sizeof(double *));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
		ptr[k][i] = (double **)malloc(rozmer_obrazku[2] * sizeof(double *));

        for (j = 0; j < rozmer_obrazku[2]; ++j){
			ptr[k][i][j] = (double *)malloc(rozmer_obrazku[3] * sizeof(double));
			for(l = 0; l < rozmer_obrazku[3]; ++l){
				int x = k-(rozmer_obrazku[0]/2);
				int y = i-(rozmer_obrazku[1]/2);
				int z = j-(rozmer_obrazku[2]/2);
				int w = l-(rozmer_obrazku[3]/2);

	//            if(abs(x*x*2 + y*y - polomer*polomer) < sirka*sirka)
				if(abs(x*x + y*y + z*z + w*w - polomer*polomer) < sirka*sirka)
					ptr[k][i][j][l] = 10.l;
				else
					ptr[k][i][j][l] = 0.l;
			}
        }
    }
}
//	vedenie_tepla(obrazok, 20);
}

void inicializuj_obrazok4d_translacia(){
    int i, j, k, l;
    const int sirka = 5;
    const int polomer = 15;

	obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double **));
	double ****ptr = (double ****)obrazok;

for(k = 0; k <rozmer_obrazku[0]; ++k){ 
	ptr[k] = (double ***)malloc(rozmer_obrazku[1] * sizeof(double *));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
		ptr[k][i] = (double **)malloc(rozmer_obrazku[2] * sizeof(double *));

        for (j = 0; j < rozmer_obrazku[2]; ++j){
			ptr[k][i][j] = (double *)malloc(rozmer_obrazku[3] * sizeof(double));
			for(l = 0; l < rozmer_obrazku[3]; ++l){
				int x = k-((rozmer_obrazku[0]-10)/2);
				int y = i-(rozmer_obrazku[1]/2);
				int z = j-(rozmer_obrazku[2]/2);
				int w = l-(rozmer_obrazku[3]/2);

	//            if(abs(x*x*2 + y*y - polomer*polomer) < sirka*sirka)
				if(l < 6 || l > rozmer_obrazku[3]-6){
					ptr[k][i][j][l] = 0.l;
					continue;
				}
				if(l==6){
					if(abs(x*x + y*y + z*z) <= polomer*polomer){
						ptr[k][i][j][l] = 10.l;
						continue;
					}
					else{
						ptr[k][i][j][l] = 0.l;
						continue;
					}
				}
	            if(l == rozmer_obrazku[3]-6){
//					x -= rozmer_obrazku[0]/2;
					x -= 10;
					if(abs(x*x + y*y + z*z) <= polomer*polomer){
						ptr[k][i][j][l] = 10.l;
						continue;
					}
					else{
						ptr[k][i][j][l] = 0.l;
						continue;
					}
				}
				if(l == 10 || l == 11){
					ptr[k][i][j][l] = 0.l;
					continue;
				}
				if(l > 6 && l < rozmer_obrazku[3]-6){
//					x -= (l-6) * (rozmer_obrazku[0]/2) / (rozmer_obrazku[3] - 6 - 6);
					x -= (l-6) * (10) / (rozmer_obrazku[3] - 6 - 6);
					if(abs(x*x + y*y + z*z - polomer*polomer) < sirka*sirka){
//					if(abs(x*x + y*y + z*z) <= polomer*polomer){
						ptr[k][i][j][l] = 10.l;
						continue;
					}
					else{
						ptr[k][i][j][l] = 0.l;
						continue;
					}
				}
			}
        }
    }
}
	vedenie_tepla(obrazok, .01);
}

void inicializuj_riesenie4d(){
    int i, j, k, l;
	const int polomer = 15;
	double tmp;
	inicializuj_riesenie4d_translacia();
	return;

	aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double ***));
	double ****ptr = (double ****)aktualne_riesenie;

for(k = 0; k<rozmer_obrazku[0]; ++k){
	ptr[k] = (double ***)malloc(rozmer_obrazku[1] * sizeof(double **));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
        ptr[k][i] = (double **)malloc(rozmer_obrazku[2] * sizeof(double *));

        for(j = 0; j < rozmer_obrazku[2]; ++j){
			ptr[k][i][j] = (double *)malloc(rozmer_obrazku[3] * sizeof(double));
			for(l = 0; l < rozmer_obrazku[3]; ++l){
	//            int x = i-(rozmer_obrazku[0]/2);
	//            int y = j-(rozmer_obrazku[1]/2);
	//            double x = k*delta[0];
	//			double y = i*delta[1];
	//			double z = j*delta[2];

				double x = k*delta[0] - .5l;
				double y = i*delta[1] - .5l;
				double z = j*delta[2] - .5l;
				double w = l*delta[3] - .5l;
//				int x = k-(rozmer_obrazku[0]/2);
//				int y = i-(rozmer_obrazku[1]/2);
//				int z = j-(rozmer_obrazku[2]/2);
//				int w = l-(rozmer_obrazku[3]/2);
//					if(x*x + y*y + z*z < polomer*polomer && abs(w) < 1){
//	                    //aktualne_riesenie[i][j] = 100.l;
//	                    ptr[k][i][j][l] = (1.l /(1.l+ sqrt(x*x+y*y+z*z)) - 1.l/(1.l+polomer))*20;
//					}
//					else{
//						ptr[k][i][j][l] = 0.l;
//					}
	//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l) + (z-.5l)*(z-.5l)-.4l*.4l);
					tmp = (x*x + y*y + z*z + w*w -.4l*.4l);
	//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.2l*.2l);
	//				tmp = ((x-.5l)*(x-.5l) + (y-.5l)*(y-.5l)-.1l*.1l);
					if(tmp > 0.l)
						ptr[k][i][j][l] = 0.l;
					else
	//					aktualne_riesenie[i][j] = tmp*tmp*100;
	//					aktualne_riesenie[i][j] = 10;
						ptr[k][i][j][l] = tmp/6.l;
			}
        }
    }
}
//	vedenie_tepla(aktualne_riesenie, 20);
}

void inicializuj_riesenie4d_translacia(){
    int i, j, k, l;
	const int polomer = 13;
	double tmp;

	aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double ***));
	double ****ptr = (double ****)aktualne_riesenie;

for(k = 0; k<rozmer_obrazku[0]; ++k){
	ptr[k] = (double ***)malloc(rozmer_obrazku[1] * sizeof(double **));
    for(i = 0; i<rozmer_obrazku[1]; ++i){
        ptr[k][i] = (double **)malloc(rozmer_obrazku[2] * sizeof(double *));

        for(j = 0; j < rozmer_obrazku[2]; ++j){
			ptr[k][i][j] = (double *)malloc(rozmer_obrazku[3] * sizeof(double));
			for(l = 0; l < rozmer_obrazku[3]; ++l){
				int x = k-((rozmer_obrazku[0]-10)/2);
				int y = i-(rozmer_obrazku[1]/2);
				int z = j-(rozmer_obrazku[2]/2);
				int w = l-(rozmer_obrazku[3]/2);
				if(l > 6 && l < rozmer_obrazku[3]-6){
//					x -= (l-6) * (rozmer_obrazku[0]/2) / (rozmer_obrazku[3] - 6 - 6);
					x -= (l-6) * (10) / (rozmer_obrazku[3] - 6 - 6);
					if(x*x + y*y + z*z < polomer*polomer){
	                    ptr[k][i][j][l] = (1.l /(1.l+ sqrt(x*x+y*y+z*z)) - 1.l/(1.l+polomer))*90;
						continue;
					}
					else{
						ptr[k][i][j][l] = 0.l;
						continue;
					}
				}
				else{
					ptr[k][i][j][l] = 0.l;
				}
			}
        }
    }
}
//	vedenie_tepla(aktualne_riesenie, 20);
}

void init4d(){
    inicializuj_obrazok4d();
    inicializuj_riesenie4d();

    int i, j;

    objemV = 1.l;
    poverchSteny = (double *)malloc(sizeof(double)*pocet_rozmerov);

    for (i = 0; i < pocet_rozmerov; ++i)
        poverchSteny[i] = 1.l;

    for (i = 0; i < pocet_rozmerov; ++i)
    {
        objemV *= delta[i];

        for (j = 0; j < pocet_rozmerov; ++j)
        {
            if(i!=j)
                poverchSteny[j] *= delta[i];
        }
    }

}
