#ifndef INIT_H
#define INIT_H
#include <stdlib.h>
#include "extern.h"
#include "malloccheck.h"
#include "matica.h"

void inicializuj_obrazok();
void inicializuj_obrazok_stvorec();
void inicializuj_riesenie();
void init();
void inicializuj_obrazok3d();
void inicializuj_riesenie3d();
void init3d();
void inicializuj_obrazok4d();
void inicializuj_obrazok4d_translacia();
void inicializuj_riesenie4d();
void inicializuj_riesenie4d_translacia();
void init4d();


#endif // INIT_H
