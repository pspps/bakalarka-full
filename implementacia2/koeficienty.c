#include "koeficienty.h"
#include "extern.h"

double f(double v){
    return v;
}

double g(POS pozicia){
    //FIXME: gredientt
    double gradient_image = abs(velkost_gradientu_na_elemente_na_druhu(obrazok, pozicia));
    return f(1/(1 + Ka * gradient_image ));
	return 1.l;
}

double v(POS pozicia, int bazovy_vektor, int hore){
	double ret;
	POS pozicia_i;
	pos_init(&pozicia_i, 0.l);
	pos_copy(&pozicia_i, &pozicia);
	pozicia_i[bazovy_vektor] += (hore?1:-1);

	ret = -wcon * poverchSteny[bazovy_vektor] * (g(pozicia_i) - g(pozicia)) / delta[bazovy_vektor];

	pos_clean(&pozicia_i);
	return ret;
}

double koeficient_prava_strana(POS pozicia){
    double riesenie, ret;
    if(pos_je_na_kraji(&pozicia))
        return 0.l;

    riesenie = *vrat_iti_riesenia(pozicia);
    ret = (objemV/deltaT) * riesenie;
//	printf("PS: %f\n", ret);

    return ret;
}

double koeficient_i(POS pozicia){
    if(pos_je_na_kraji(&pozicia))
        return 1.0;

    double ret = 0.l,
		   tmp_v = 0.l;

    double casovy_koeficient = 0.l,
           konveksny_koeficient = 0.l,
           difuzny_koeficient = 0.l;

    casovy_koeficient = objemV/deltaT;

    int bazovy_vektor, hore;
    POS i;
    pos_init(&i, 0.l);
    pos_copy(&i, &pozicia);
	//pos_print(&pozicia);
	//printf("-------------------------\n");

    for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor)
    {
        for (hore = 0; hore < 2; ++hore)
        {
            //pos_copy(&i, &pozicia);
            i[bazovy_vektor] += (hore ? 1 : -1);
			tmp_v = v(pozicia, bazovy_vektor, hore);

			if(tmp_v<0){
				konveksny_koeficient -= tmp_v;
			}
	        difuzny_koeficient += poverchSteny[bazovy_vektor]/
				(delta[bazovy_vektor] * sqrt(epsilon*epsilon + 
				velkost_gradientu_na_hrane_na_druhu(aktualne_riesenie, pozicia, bazovy_vektor, hore)));
//			printf("%f %f\n", velkost_gradientu_na_hrane_na_druhu(aktualne_riesenie, i, bazovy_vektor, hore), difuzny_koeficient);
//			printf("~~~gradient na hrane: %f\n", velkost_gradientu_na_hrane_na_druhu(aktualne_riesenie,pozicia, bazovy_vektor, hore));

	        //i[bazovy_vektor] = pozicia[bazovy_vektor];
            i[bazovy_vektor] -= (hore ? 1 : -1);
        }
    }
    pos_clean(&i);

	//printf("%f %f\n", difuzny_koeficient, (wdif * g(pozicia) * sqrt(epsilon*epsilon + velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia))));
//	printf("~~~~%f\n", wdif * g(pozicia) * sqrt(epsilon*epsilon + velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia)));

    difuzny_koeficient *= wdif * g(pozicia) * 
		sqrt(epsilon*epsilon + velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia));
//	printf("---gradient na elemente: %f\n", velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia));


    ret = casovy_koeficient + konveksny_koeficient + difuzny_koeficient;
//	printf("%f ---- %f ---- %f\n", casovy_koeficient, konveksny_koeficient, difuzny_koeficient);
	//printf("%f ---- %f\n", konveksny_koeficient, difuzny_koeficient);
	//printf("%f v i\n", ret);

    return ret;
}

double koeficient_iplus(POS pozicia, int bazovy_vektor, int hore){
    if(pos_je_na_kraji(&pozicia))
        return 0.l;

    double ret = 0.l,
		   tmp_v = 0.l;

    double konveksny_koeficient = 0.l,
           difuzny_koeficient = 0.l;

    double tmp_gradient;

    POS i;
    pos_init(&i, 0.l);
    pos_copy(&i, &pozicia);
    i[bazovy_vektor] += (hore ? 1 : -1);

	tmp_v = v(pozicia, bazovy_vektor, hore);

	if(tmp_v<0){
		konveksny_koeficient += tmp_v;
	}

    tmp_gradient = velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia);
    difuzny_koeficient = -wdif * g(pozicia) * sqrt(epsilon*epsilon + tmp_gradient) * poverchSteny[bazovy_vektor] 
		/ (delta[bazovy_vektor] * sqrt(epsilon*epsilon + velkost_gradientu_na_hrane_na_druhu (aktualne_riesenie, pozicia, bazovy_vektor, hore)));

    pos_clean(&i);

    ret = konveksny_koeficient + difuzny_koeficient;

	//printf("%f v ipp\n", ret);
//	printf("%f ---- %f\n", konveksny_koeficient, difuzny_koeficient);
    return ret;
}
