#ifndef KOEFICIENTY_H
#define KOEFICIENTY_H
#include <math.h>
#include "pos.h"
#include "matica.h"
#include "malloccheck.h"

double g(POS pozicia);
double koeficient_i(POS pozicia);
double koeficient_iplus(POS pozicia, int bazovy_vektor, int hore);
double koeficient_prava_strana(POS pozicia);


#endif // KOEFICIENTY_H
