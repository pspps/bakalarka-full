#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#define EXTERN_H
#include "pos.h"
#include "matica.h"
#include "init.h"
#include "malloccheck.h"
#include "koeficienty.h"

#define obrazokX 50
#define obrazokY 50
#define obrazokZ 50
#define obrazokW 50

const unsigned int pocet_rozmerov = 4;
double delta[] = {1.l, 1.l, 1.l, 1.l};
double deltaT = 1.l;
int rozmer_obrazku[] = {obrazokX, obrazokY, obrazokZ, obrazokW};
unsigned int pocet_iteracii = 200;
int pocet_bodov = 1;

double objemV;
double *poverchSteny;

//TODO: 2D
double **obrazok;
double **aktualne_riesenie;

double wcon = 1.l,
       wdif = 1.l,
       epsilon = 1.l,
       Ka = 8.l;

int n;
double h;

double SOR_koeficient = 1.7l;

int main(void)
{
	n = 50;
	h = 1.l/(double)n;
	h = 1;
	deltaT = 1.l;
//	deltaT /= 4;
	deltaT = h;
	deltaT = 1.l;
	epsilon = h*h;
	epsilon = 1;
//	epsilon = .0001l;
//	pocet_iteracii = (int)(.04/deltaT)+1;
	pocet_iteracii = 160;
	delta[0] = h;
	delta[1] = h;
	delta[2] = h;
	delta[3] = h;
	rozmer_obrazku[0] = (n+1)+10;
//	rozmer_obrazku[0] = (n+1);
	rozmer_obrazku[1] = n+1;
	rozmer_obrazku[2] = n+1;
	rozmer_obrazku[3] = n+1;
	rozmer_obrazku[3] = 21;

    unsigned int cislo_riesenia;
    char *nazov_suboru;
    double /* **matica, */*prava_strana, *riesenie, tmp_double;
	double *diagonala, **vedlajsie_diagonaly;
    int i, j, k;
	int iteracie_gauss_seidel, i_gauss, j_gauss, bazovy_vektor, hore;
	double sigma, SOR_chyba;
	double *docastny_vektor;
    POS i_pos, tmp_pos;
    cyklus_cez_rozmery//MAKRO KROTE SA PRELOZI NA CIKLUS
        pocet_bodov *= rozmer_obrazku[i];
	docastny_vektor = (double *)malloc(sizeof(double)*pocet_bodov);

    printf("Ahoj svet\n");
	printf("pocet rozmerov: %d\n", pocet_rozmerov);
	printf("pocet prvkov siete: %d\n", n);
	printf("delta t: %f\n", deltaT);
	printf("h: %f\n", h);
	printf("epsilon: %f\n", epsilon);
	if(pocet_rozmerov == 2) {
	    init();
	}
	else if(pocet_rozmerov == 3) {
	    init3d();
	}
	else if(pocet_rozmerov == 4) {
	    init4d();
	}

    nazov_suboru = (char *)malloc(strlen("data/riesenie.00000.matica")*sizeof(char));
	uloz_riesenie(0);
	uloz_obrazok();

//    exportuj_maticu(obrazok, "data/obrazok.matica");
//    exportuj_maticu(aktualne_riesenie, "data/riesenie.0.matica");

    pos_init_viacero(2, 0.l, &i_pos, &tmp_pos);

	//alokacia matice
//    double *matica_ptr = malloc(sizeof(double) * pocet_bodov * pocet_bodov);
//    matica = (double **)malloc(sizeof(double *) * pocet_bodov);

	//alokacia pre gauss seidela
	diagonala = malloc(sizeof(double)*pocet_bodov);
	for(i=0; i<pocet_bodov; ++i){
		diagonala[i] = 0.l;
	}
	vedlajsie_diagonaly = malloc(sizeof(double *)*pocet_rozmerov*2);
	for(i=0; i<2*pocet_rozmerov; ++i){
		vedlajsie_diagonaly[i] = malloc(sizeof(double)*pocet_bodov);
		for(j=0; j<pocet_bodov; ++j){
			vedlajsie_diagonaly[i][j] = 0.l;
		}
	}

    prava_strana = (double *)malloc(sizeof(double) * pocet_bodov);
    riesenie = (double *)malloc(sizeof(double) * pocet_bodov);

/*    for(i = 0; i<pocet_bodov; ++i){
        //matica[i] = (double *)malloc(sizeof(double) * pocet_bodov);
        matica[i] = matica_ptr + (i*pocet_bodov);
        for(j = 0; j < pocet_bodov; ++j)
            matica[i][j] = 0.l;
    }*/

    printf("Inicializacia skoncena\n");
    fflush(stdout);

	printf("------------------------------------------------------\n");
	printf("VYPOCET\n");
	printf("------------------------------------------------------\n");

    for(cislo_riesenia = 1; cislo_riesenia <= pocet_iteracii; ++cislo_riesenia){
    //for(cislo_riesenia = 1; cislo_riesenia <= 0; ++cislo_riesenia){
        printf("Zacinam %04d tu iteraciu\n", cislo_riesenia);
        fflush(stdout);
        for(i = 0; i<pocet_bodov; ++i){
            index_to_pos(&i_pos, i);

			//Plna matica
/*            if(pos_je_na_kraji(&i_pos)){
                matica[i][i] = 1.l;
                //TODO
                prava_strana[i] = 0.l;
                continue;
            }*/
			if(pos_je_na_kraji(&i_pos)){
				diagonala[i] = 1.l;
                //TODO
                prava_strana[i] = 0.l;
                continue;
            }

            pos_copy(&tmp_pos, &i_pos);
            prava_strana[i] = koeficient_prava_strana(i_pos);

			//Plna matica
/*            matica[i][i] = koeficient_i(i_pos);

            for(j = 0; j < pocet_rozmerov; ++j){
                tmp_pos[j] += 1;
                matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 1);
                tmp_pos[j] -= 2;
                matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 0);
            }*/

			//Gaus seidel
			diagonala[i] = koeficient_i(i_pos);
            for(j = 0; j < pocet_rozmerov; ++j){
                vedlajsie_diagonaly[2*j+1][i] = koeficient_iplus(i_pos, j, 1);
				vedlajsie_diagonaly[2*j][i] = koeficient_iplus(i_pos, j, 0);
			}

        }
        printf("Matica vytvorena\n");
        fflush(stdout);
        //TODO: vymazat
        sprintf(nazov_suboru, "data/mmmmmmatica.%d.matica", cislo_riesenia);
        //exportuj_maticu_inu(matica, nazov_suboru, pocet_bodov);

        //memcpy(riesenie, aktualne_riesenie, sizeof(double)*pocet_bodov);
		for(i=0; i<pocet_bodov; ++i){
            index_to_pos(&i_pos, i);
			riesenie[i] = *vrat_iti_riesenia(i_pos);
		}
		//kopiruj_maticu(riesenie, aktualne_riesenie);
        //gnu_solve(matica_ptr, prava_strana, riesenie, pocet_bodov);
        //gauss_seidel(pocet_bodov, matica, prava_strana, riesenie);

//////////////////////////////////////////////
//               GAUSE SEIDEL               //
//////////////////////////////////////////////
//		//riesenie sustavy gauss seidelom
//		for(iteracie_gauss_seidel=0; iteracie_gauss_seidel<pocet_iteracii_gauss_seidela; ++iteracie_gauss_seidel){
//			for(i_gauss=0; i_gauss < pocet_bodov; ++i_gauss){
//				sigma = 0.l;
//				for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor){
//            		index_to_pos(&i_pos, i_gauss);
//					//FIXME
//					if(pos_je_na_kraji(&i_pos)){
//						continue;
//					}
//					i_pos[bazovy_vektor] += 1;
//					sigma += vedlajsie_diagonaly[2*bazovy_vektor+1][i_gauss] * riesenie[pos_to_index(&i_pos)];
//					i_pos[bazovy_vektor] -= 2;
//					sigma += vedlajsie_diagonaly[2*bazovy_vektor][i_gauss] * riesenie[pos_to_index(&i_pos)];
//				}
////				if(abs(riesenie[i_gauss] - (prava_strana[i_gauss]-sigma)/diagonala[i_gauss]) > .01)
////					printf("gaus sajdel(s,ps,diag, st) %f %f %f %f\n", sigma, prava_strana[i_gauss], diagonala[i_gauss], riesenie[i_gauss]);
//				riesenie[i_gauss] = (prava_strana[i_gauss]-sigma)/diagonala[i_gauss];
//			}
//		}

//////////////////////////////////////////////
//                  SOR                     //
//////////////////////////////////////////////
		//riesenie sustavy gauss seidelom
		memcpy(docastny_vektor, aktualne_riesenie, sizeof(double)*pocet_bodov);
		for(iteracie_gauss_seidel=0; iteracie_gauss_seidel<pocet_iteracii_gauss_seidela; ++iteracie_gauss_seidel){
			SOR_chyba = 0.l;
			for(i_gauss=0; i_gauss < pocet_bodov; ++i_gauss){
				sigma = 0.l;
				for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor){
            		index_to_pos(&i_pos, i_gauss);
					//FIXME
					if(pos_je_na_kraji(&i_pos)){
						continue;
					}
					i_pos[bazovy_vektor] += 1;
					sigma += vedlajsie_diagonaly[2*bazovy_vektor+1][i_gauss] * riesenie[pos_to_index(&i_pos)];
					i_pos[bazovy_vektor] -= 2;
					sigma += vedlajsie_diagonaly[2*bazovy_vektor][i_gauss] * riesenie[pos_to_index(&i_pos)];
				}
//				if(abs(riesenie[i_gauss] - (prava_strana[i_gauss]-sigma)/diagonala[i_gauss]) > .01)
//					printf("gaus sajdel(s,ps,diag, st) %f %f %f %f\n", sigma, prava_strana[i_gauss], diagonala[i_gauss], riesenie[i_gauss]);
//				double ttmmpp = 
				riesenie[i_gauss] = (1.l-SOR_koeficient)*riesenie[i_gauss] + SOR_koeficient*(prava_strana[i_gauss]-sigma)/diagonala[i_gauss];
				sigma += diagonala[i_gauss]*riesenie[i_gauss];
				SOR_chyba += (prava_strana[i_gauss]-sigma)*(prava_strana[i_gauss]-sigma);
			}
			if(sqrt(SOR_chyba) > .00001){
				//printf("L2 SOR chyba: %f\n", sqrt(SOR_chyba));
				continue;
			}
			break;
		}
		if(iteracie_gauss_seidel == pocet_iteracii_gauss_seidela){
			printf("CHYBA: SOR neskonvergoval...\n");
			printf("L2 SOR chyba: %f\n", sqrt(SOR_chyba));
			fflush(stdout);
		}

        printf("Matica vyriesena v %d krokoch\n", iteracie_gauss_seidel+1);
        fflush(stdout);

        for(i = 0; i<pocet_bodov; ++i){
            index_to_pos(&tmp_pos, i);
            (*vrat_iti_element(aktualne_riesenie, tmp_pos)) = riesenie[i];
        }
		double ttttttd, L2chyba = 0.l;
        for(i = 0; i<pocet_bodov; ++i){
            index_to_pos(&tmp_pos, i);

//			pos_print(&tmp_pos);

            double x = tmp_pos[0]*delta[0] - .5l;
			double y = tmp_pos[1]*delta[1] - .5l;
			double z, w;
			if(pocet_rozmerov > 2){
				z = tmp_pos[2]*delta[2] - .5l;
			}
			if(pocet_rozmerov > 3){
				w = tmp_pos[3]*delta[3] - .5l;
			}


			if(pocet_rozmerov == 2) {
				ttttttd = x*x + y*y;
				ttttttd = (ttttttd - .4l*.4l)/2.l + deltaT*cislo_riesenia;
			}
			else if(pocet_rozmerov == 3) {
				ttttttd = x*x + y*y + z*z;
				ttttttd = (ttttttd - .4l*.4l)/4.l + deltaT*cislo_riesenia;
			}
			else if(pocet_rozmerov == 4) {
				ttttttd = x*x + y*y + z*z + w*w;
				ttttttd = (ttttttd - .4l*.4l)/6.l + deltaT*cislo_riesenia;
			}
			ttttttd = ttttttd < 0.l ? ttttttd : 0.l;

			ttttttd = riesenie[i] - ttttttd;
			L2chyba += ttttttd*ttttttd;
        }
		L2chyba *= objemV;
		L2chyba = sqrt(L2chyba);

		printf("L2 chyba je: %f\n", L2chyba);

		uloz_riesenie(cislo_riesenia);
//        sprintf(nazov_suboru, "data/riesenie.%d.matica", cislo_riesenia);
//        exportuj_maticu(aktualne_riesenie, nazov_suboru);
//		printf("Subor '%s' zapisany\n", nazov_suboru);
//        sprintf(nazov_suboru, "data/riesenieNormovane.%d.matica", cislo_riesenia);
//        exportuj_maticu_normovanu(aktualne_riesenie, nazov_suboru);
//		printf("Subor '%s' zapisany\n", nazov_suboru);
//        sprintf(nazov_suboru, "data/matica.%d.matica", cislo_riesenia);
////        exportuj_maticu_inu(matica, nazov_suboru, pocet_bodov);
//		exportuj_maticu_gaussajdel(diagonala, vedlajsie_diagonaly, prava_strana, nazov_suboru);
//		printf("Subor '%s' zapisany\n", nazov_suboru);
//        printf("Koncim  %04d tu iteraciu\n", cislo_riesenia);
//        sprintf(nazov_suboru, "data/matica.%d.matica", cislo_riesenia);
////        exportuj_maticu_inu(matica, nazov_suboru, pocet_bodov);
//		exportuj_maticu_gaussajdel(diagonala, vedlajsie_diagonaly, prava_strana, nazov_suboru);
        printf("Koncim  %04d tu iteraciu\n", cislo_riesenia);
        fflush(stdout);
    }

    printf("Koniec :)\n");

    return 0;
}

