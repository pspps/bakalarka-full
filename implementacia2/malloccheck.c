#include <stdlib.h>
#include <stdio.h>

void *malloc_safe(size_t v){
    void *ret;
    ret = malloc(v);

    if(!ret){
        perror("Nepodarilo sa mi alokovat premennu\n");
    }
    return ret;
}
