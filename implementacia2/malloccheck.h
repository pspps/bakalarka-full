#ifndef MALLOCCHECK_C
#define MALLOCCHECK_C
#include <stdio.h>

#define malloc malloc_safe
void *malloc_safe(size_t v);

#endif // MALLOCCHECK_C

