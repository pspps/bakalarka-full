#include "matica.h"
#include "extern.h"

double *vrat_iti_obrazka(POS pozicia){
    return vrat_iti_element(obrazok, pozicia);
}

double *vrat_iti_riesenia(POS pozicia){
    return vrat_iti_element(aktualne_riesenie, pozicia);
}

double *vrat_iti_element(double **pole, POS pozicia){
    int i;
    double **pointer = pole;
	static const double nulovy_doble = 0.l;

    for(i = 0; i<pocet_rozmerov; ++i)
        if(pozicia[i] < 0 || pozicia[i] >= rozmer_obrazku[i])
            return &nulovy_doble;

    for (i = 0; i < pocet_rozmerov-1; ++i)
    {
        pointer = (double **)pointer[pozicia[i]];
    }

    pointer = (double **) ((double *)pointer + pozicia[pocet_rozmerov-1]);

    return (double *)pointer;
}

double *gradient_na_hrane(double **pole, POS pozicia, int bazovy_vektor, int hore){
    static double *ret = NULL;
	if(!ret){
		ret = (double *)malloc(sizeof(double)*pocet_rozmerov);
	}
    int i;

    for(i=0; i<pocet_rozmerov; ++i)
        ret[i] = 0.l;

    static POS pozicia_nove=NULL, pozicia_horne=NULL, pozicia_dolne=NULL;
	if(!pozicia_nove){
	    pos_init_viacero(3, 0.l, &pozicia_nove, &pozicia_dolne, &pozicia_horne);
	}
    /*pos_init(&pozicia_nove, 0.l);
    pos_init(&pozicia_dolne, 0.l);
    pos_init(&pozicia_horne, 0.l);*/

    pos_copy(&pozicia_horne, &pozicia);
    pos_copy(&pozicia_dolne, &pozicia);
    for(i=0; i<pocet_rozmerov; i++){
        if(i == bazovy_vektor){
            pos_copy(&pozicia_nove, &pozicia);
            pozicia_nove[i] += ( hore ? 1 : -1);

            ret[i] = (*vrat_iti_element(pole, pozicia_nove) - *vrat_iti_element(pole, pozicia)) / delta[i];
            continue;
        }

//        pozicia_dolne[i] += -1;
//        pozicia_horne[i] += 1;
//
//        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
//
//        pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
//        pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);
//
//        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
//
//        ret[i] /= 4*delta[i];
        pozicia_dolne[i] += -1;
        pozicia_horne[i] += 1;

        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);

        pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
        pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);

        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
        pozicia_dolne[i] -= -1;
        pozicia_horne[i] -= 1;
        pozicia_dolne[bazovy_vektor] -= (hore ? 1 : -1);
        pozicia_horne[bazovy_vektor] -= (hore ? 1 : -1);

        ret[i] /= 4*delta[i];
    }

//    pos_clean_viacero(3, &pozicia_nove, &pozicia_dolne, &pozicia_horne);

    return ret;
}

double velkost_gradientu_na_hrane_na_druhu(double **pole, POS pozicia, int bazovy_vektor, int hore){
    double ret = 0.l, tmp;
    int i;

    static POS pozicia_nove=NULL, pozicia_horne=NULL, pozicia_dolne=NULL;
	if(!pozicia_nove){
	    pos_init_viacero(3, 0.l, &pozicia_nove, &pozicia_dolne, &pozicia_horne);
	}
    /*pos_init(&pozicia_nove, 0.l);
    pos_init(&pozicia_dolne, 0.l);
    pos_init(&pozicia_horne, 0.l);*/

    pos_copy(&pozicia_horne, &pozicia);
    pos_copy(&pozicia_dolne, &pozicia);
    for(i=0; i<pocet_rozmerov; i++){
        if(i == bazovy_vektor){
            pos_copy(&pozicia_nove, &pozicia);
            pozicia_nove[i] += ( hore ? 1 : -1);

            tmp = (*vrat_iti_element(pole, pozicia_nove) - *vrat_iti_element(pole, pozicia)) / delta[i];
			ret += tmp*tmp;
            continue;
        }

//        pozicia_dolne[i] += -1;
//        pozicia_horne[i] += 1;
//
//        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
//
//        pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
//        pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);
//
//        ret[i] += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
//
//        ret[i] /= 4*delta[i];
        pozicia_dolne[i] += -1;
        pozicia_horne[i] += 1;

        tmp = *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);

        pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
        pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);

        tmp += *vrat_iti_element(pole, pozicia_horne) - *vrat_iti_element(pole, pozicia_dolne);
        pozicia_dolne[i] -= -1;
        pozicia_horne[i] -= 1;
        pozicia_dolne[bazovy_vektor] -= (hore ? 1 : -1);
        pozicia_horne[bazovy_vektor] -= (hore ? 1 : -1);

        tmp /= 4*delta[i];
		ret += tmp*tmp;
    }

//    pos_clean_viacero(3, &pozicia_nove, &pozicia_dolne, &pozicia_horne);

    return ret;
}
//FIXME SCITAVAT NORMI, NIE VEKTORI
//double *gradient_na_elemente(double **pole, POS pozicia){
//    double *ret = (double *)malloc(sizeof(double)*pocet_rozmerov),
//           *tmp_double;
//    int i, j;
//
//    for(i=0; i<pocet_rozmerov; ++i)
//        ret[i] = 0.l;
//
//    for(i=0; i<2*pocet_rozmerov; ++i){
//        tmp_double = gradient_na_hrane(pole, pozicia, i>>1, i&1);
//
//        for(j=0; j<pocet_rozmerov; ++j)
//            ret[j] += tmp_double[j];
//
//        free(tmp_double);
//    }
//
//    for(j=0; j<pocet_rozmerov; ++j)
//        ret[j] /= 2*pocet_rozmerov;
//
//    return ret;
//}

double velkost_gradientu_na_elemente_na_druhu(double **pole, POS pozicia){
    double *tmp,
           ret = 0.l;
    int i;

//    tmp = gradient_na_elemente(pole, pozicia);    
//    for(i=0; i<pocet_rozmerov; ++i)    
//        ret += tmp[i]*tmp[i];        
	for(i=0; i<pocet_rozmerov; ++i){
		ret += velkost_gradientu_na_hrane_na_druhu(pole, pozicia, i, 0);
		ret += velkost_gradientu_na_hrane_na_druhu(pole, pozicia, i, 1);
	}

    //free(tmp);
	ret /= 2*pocet_rozmerov;

    return ret;
}

void exportuj_maticu(double **pole, char *nazov_suboru){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
    for(i = 0; i < rozmer_obrazku[0]; ++i){
        for(j = 0; j < rozmer_obrazku[1]; ++j){
            fprintf(f, "%lf ", pole[i][j]);
        }
        putc('\n', f);
    }
	fflush(f);
	fclose(f);
}

void exportuj_maticu_normovanu(double **pole, char *nazov_suboru){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
	double suma = 0.l;
    for(i = 0; i < rozmer_obrazku[0]; ++i){
        for(j = 0; j < rozmer_obrazku[1]; ++j){
			double tmp = ((i*delta[0]-.5l)*(i*delta[0]-.5l) + (j*delta[1]-.5l)*(j*delta[1]-.5l)-.4l*.4l)/2.l + .04l;
			//printf("%f %f\n", pole[i][j], tmp);
			tmp = (tmp < 0.l ? tmp : 0.l);
			tmp -= pole[i][j];
			tmp = tmp * tmp;
			tmp *= objemV;
            fprintf(f, "%.10lf ", tmp);
			suma += tmp;
        }
        putc('\n', f);
    }
	//printf("L2 chyba: %f\n", sqrt(suma));
	fflush(f);
	fclose(f);
}

void exportuj_maticu_gaussajdel(double *diagonala, double **vedlajsie, double *prava, char *nazov_suboru){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
    for(i = 0; i < pocet_bodov; ++i){
        fprintf(f, "%f %f ", prava[i], diagonala[i]);
        for(j = 0; j < 2*pocet_rozmerov; ++j){
            fprintf(f, "%f ", vedlajsie[j][i]);
        }
        putc('\n', f);
    }
	fflush(f);
	fclose(f);
}

void exportuj_maticu_inu(double **pole, char *nazov_suboru, unsigned int velkost){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
    for(i = 0; i < velkost; ++i){
        for(j = 0; j < velkost; ++j){
            fprintf(f, "%lf ", pole[i][j]);
        }
        putc('\n', f);
    }
	fflush(f);
	fclose(f);
}

void uloz_riesenie(int cislo_riesenia){
	int i, j, k, l;
	char nazov[40];
	FILE *f;

	if(pocet_rozmerov == 2){
		sprintf((char *)nazov, "data/riesenie.%d.matica", cislo_riesenia);
		f = fopen(nazov, "w");

		for(i = 0; i<rozmer_obrazku[0]; ++i){
			for(j = 0; j<rozmer_obrazku[1]; ++j){
				fprintf(f, "%.8f ", aktualne_riesenie[i][j]);
			}
			fprintf(f, "\n");
		}
		fflush(f);
		fclose(f);
		printf("Subor `%s` zapisany\n", (char *)nazov);
	}

	else if(pocet_rozmerov == 3){
		double ***ptr = (double ***)aktualne_riesenie;
		sprintf((char *)nazov, "data/riesenie.%d.vtk", cislo_riesenia);
		f = fopen(nazov, "w");

		fprintf(f, "# vtk DataFile Version 2.0\n");
		fprintf(f, "vtk output\n");
		fprintf(f, "ASCII\n");
		fprintf(f, "DATASET STRUCTURED_POINTS\n");
		fprintf(f, "DIMENSIONS %d %d %d\n", rozmer_obrazku[0], rozmer_obrazku[1], rozmer_obrazku[2]);
		fprintf(f, "SPACING 1 1 1\n");
		fprintf(f, "ORIGIN 0 0 0\n");
		fprintf(f, "POINT_DATA %d\n", pocet_bodov); // total number of points on structured data.
		fprintf(f, "SCALARS ImageFile double 1\n");
		fprintf(f, "LOOKUP_TABLE default\n");

		for(i = 0; i<rozmer_obrazku[0]; ++i){
			for(j = 0; j<rozmer_obrazku[1]; ++j){
				for(k = 0; k<rozmer_obrazku[2]; ++k){
					fprintf(f, "%.8f ", ptr[i][j][k]);
				}
				fprintf(f, "\n");
			}
			fprintf(f, "\n");
		}
		fflush(f);
		fclose(f);
		printf("Subor `%s` zapisany\n", (char *)nazov);
	}

	else if(pocet_rozmerov == 4){
		double ****ptr = (double ****)aktualne_riesenie;
		for(l = 0; l<rozmer_obrazku[3]; ++l){
			sprintf((char *)nazov, "data/riesenie.%d.cas.%d.vtk", cislo_riesenia, l);
			f = fopen(nazov, "w");

			fprintf(f, "# vtk DataFile Version 2.0\n");
			fprintf(f, "vtk output\n");
			fprintf(f, "ASCII\n");
			fprintf(f, "DATASET STRUCTURED_POINTS\n");
			fprintf(f, "DIMENSIONS %d %d %d\n", rozmer_obrazku[2], rozmer_obrazku[1], rozmer_obrazku[0]);
			fprintf(f, "SPACING 1 1 1\n");
			fprintf(f, "ORIGIN 0 0 0\n");
			fprintf(f, "POINT_DATA %d\n", pocet_bodov/ rozmer_obrazku[3]); // total number of points on structured data.
			fprintf(f, "SCALARS ImageFile double 1\n");
			fprintf(f, "LOOKUP_TABLE default\n");

			for(i = 0; i<rozmer_obrazku[0]; ++i){
				for(j = 0; j<rozmer_obrazku[1]; ++j){
					for(k = 0; k<rozmer_obrazku[2]; ++k){
						fprintf(f, "%.8f ", ptr[i][j][k][l]);
					}
					fprintf(f, "\n");
				}
				fprintf(f, "\n");
			}
			fflush(f);
			fclose(f);
			printf("Subor `%s` zapisany\n", (char *)nazov);
			fflush(stdout);
		}
	}
}

void uloz_obrazok(){
	int i, j, k, l;
	char nazov[40];
	FILE *f;

	if(pocet_rozmerov == 2){
		sprintf((char *)nazov, "data/obrazok.matica");
		f = fopen(nazov, "w");

		for(i = 0; i<rozmer_obrazku[0]; ++i){
			for(j = 0; j<rozmer_obrazku[1]; ++j){
				fprintf(f, "%.8f ", obrazok[i][j]);
			}
			fprintf(f, "\n");
		}
		fflush(f);
		fclose(f);
		printf("Subor `%s` zapisany\n", (char *)nazov);
	}

	else if(pocet_rozmerov == 3){
		double ***ptr = (double ***)obrazok;
		sprintf((char *)nazov, "data/obrazok.vtk");
		f = fopen(nazov, "w");

		fprintf(f, "# vtk DataFile Version 2.0\n");
		fprintf(f, "vtk output\n");
		fprintf(f, "ASCII\n");
		fprintf(f, "DATASET STRUCTURED_POINTS\n");
		fprintf(f, "DIMENSIONS %d %d %d\n", rozmer_obrazku[2], rozmer_obrazku[1], rozmer_obrazku[0]);
		fprintf(f, "SPACING 1 1 1\n");
		fprintf(f, "ORIGIN 0 0 0\n");
		fprintf(f, "POINT_DATA %d\n", pocet_bodov); // total number of points on structured data.
		fprintf(f, "SCALARS ImageFile double 1\n");
		fprintf(f, "LOOKUP_TABLE default\n");

		for(i = 0; i<rozmer_obrazku[0]; ++i){
			for(j = 0; j<rozmer_obrazku[1]; ++j){
				for(k = 0; k<rozmer_obrazku[2]; ++k){
					fprintf(f, "%.8f ", ptr[i][j][k]);
				}
				fprintf(f, "\n");
			}
			fprintf(f, "\n");
		}
		fflush(f);
		fclose(f);
		printf("Subor `%s` zapisany\n", (char *)nazov);
	}

	else if(pocet_rozmerov == 4){
		double ****ptr = (double ****)obrazok;
		for(l = 0; l<rozmer_obrazku[3]; ++l){
			sprintf((char *)nazov, "data/obrazok.cas.%d.vtk", l);
			f = fopen(nazov, "w");

			fprintf(f, "# vtk DataFile Version 2.0\n");
			fprintf(f, "vtk output\n");
			fprintf(f, "ASCII\n");
			fprintf(f, "DATASET STRUCTURED_POINTS\n");
			fprintf(f, "DIMENSIONS %d %d %d\n", rozmer_obrazku[2], rozmer_obrazku[1], rozmer_obrazku[0]);
			fprintf(f, "SPACING 1 1 1\n");
			fprintf(f, "ORIGIN 0 0 0\n");
			fprintf(f, "POINT_DATA %d\n", pocet_bodov/ rozmer_obrazku[3]); // total number of points on structured data.
			fprintf(f, "SCALARS ImageFile double 1\n");
			fprintf(f, "LOOKUP_TABLE default\n");

			for(i = 0; i<rozmer_obrazku[0]; ++i){
				for(j = 0; j<rozmer_obrazku[1]; ++j){
					for(k = 0; k<rozmer_obrazku[2]; ++k){
						fprintf(f, "%.8f ", ptr[i][j][k][l]);
					}
					fprintf(f, "\n");
				}
				fprintf(f, "\n");
			}
			fflush(f);
			fclose(f);
			printf("Subor `%s` zapisany\n", (char *)nazov);
			fflush(stdout);
		}
	}
}

void alokuj_maticu_implementacia(double **v, int hlbka){
//    int i;
//
//    if(hlbka == pocet_rozmerov-1){
//        *v = (double *)malloc(sizeof(double) * rozmer_obrazku[pocet_rozmerov-1]);
//
//        for(i = 0; i<rozmer_obrazku[hlbka]; ++i)
//            (*v)[i] = 0.l;
//
//        return;
//    }
//
//    *v = (double *)malloc(sizeof(double *) * rozmer_obrazku[hlbka]);
//
//    for(i = 0; i<rozmer_obrazku[hlbka]; ++i){
//        alokuj_maticu_implementacia((double **)&(*v)[i], hlbka+1);
//    }
	int i, j, k, l;
	if(pocet_rozmerov == 2){
		*v = (double *)malloc(sizeof(double *) * rozmer_obrazku[0]);
		for(j = 0; j<rozmer_obrazku[0]; ++j){
			((double **)(*v))[j] = (double *)malloc(sizeof(double) * rozmer_obrazku[1]);
		}
	}
	if(pocet_rozmerov == 3){
		*v = (double *)malloc(sizeof(double *) * rozmer_obrazku[0]);
		for(j = 0; j<rozmer_obrazku[0]; ++j){
			((double ***)(*v))[j] = (double **)malloc(sizeof(double *) * rozmer_obrazku[1]);
			for(i = 0; i<rozmer_obrazku[1]; ++i){
				((double ***)(*v))[j][i] = (double *)malloc(sizeof(double) * rozmer_obrazku[2]);
			}
		}
	}
	if(pocet_rozmerov == 4){
		*v = (double *)malloc(sizeof(double *) * rozmer_obrazku[0]);
		double ****ptr = *v;

		for(j = 0; j<rozmer_obrazku[0]; ++j){
			ptr[j] = (double **)malloc(sizeof(double *) * rozmer_obrazku[1]);
			for(i = 0; i<rozmer_obrazku[1]; ++i){
				ptr[j][i] = (double *)malloc(sizeof(double *) * rozmer_obrazku[2]);
				for(k = 0; k<rozmer_obrazku[2]; ++k){
				ptr[j][i][k] = (double *)malloc(sizeof(double) * rozmer_obrazku[3]);
				}
			}
		}
	}
}

void kopiruj_maticu(double **ciel, double **zdroj){
	int i;
	POS pozicia;
	pos_init(&pozicia, 0.l);

	for(i = 0; i<pocet_bodov; ++i){
		index_to_pos(&pozicia, i);
		*vrat_iti_element(ciel, pozicia) = *vrat_iti_element(zdroj, pozicia);
	}

	pos_clean(&pozicia);
}

void vycisti_maticu_implementacia(double **v, int hlbka){
    int i;

	if(hlbka < pocet_rozmerov - 1){
		for(i = 0; i<rozmer_obrazku[hlbka]; ++i){
			vycisti_maticu_implementacia((double **)v[i], hlbka + 1);
		}
	}
	
	free(v);
}

void gauss_seidel(int dlzka, double **matica_v, double *ps, double *riesenie){
    int i, j, iteracie;
    double sigma;

    for(iteracie = 0; iteracie < pocet_iteracii_gauss_seidela; ++iteracie){
        for(i = 0; i<dlzka; ++i){
            sigma = 0.l;

            for(j = 0; j<dlzka; ++j){
                if(i!=j){
                    sigma += matica_v[i][j] * riesenie[j];
                }
            }

            riesenie[i] = (ps[i] - sigma) / matica_v[i][i];
        }
//        printf("%f %f %f %f %f\n", riesenie[0], riesenie[1], riesenie[2], riesenie[3], riesenie[4]);
    }
}

//double *gnu_solve(double *matica_v, double *prava_v, double *ciel, int velkost){
//    gsl_matrix_view m
//      = gsl_matrix_view_array (matica_v, velkost, velkost);
//
//    gsl_vector_view b
//      = gsl_vector_view_array (prava_v, velkost);
//
//    gsl_vector *x = gsl_vector_alloc (velkost);
//
//    int s;
//
//    gsl_permutation * p = gsl_permutation_alloc (velkost);
//
//    gsl_linalg_LU_decomp (&m.matrix, p, &s);
//
//    gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);
//
//    memcpy(ciel, gsl_vector_ptr(x, 0), sizeof(double) * velkost);
//
//    gsl_permutation_free (p);
//    gsl_vector_free (x);
//}

void vedenie_tepla(double **matica, double cas){
	int i, j, iteracie, bazovy_vektor, hore;
	int  pokracovat = 0;
	double sigma, **kopia_matice, tmp_double;
	POS pozicia, pozicia_i;
	pos_init(&pozicia, 0.l);
	pos_init(&pozicia_i, 0.l);
	alokuj_maticu((double **)&kopia_matice);
//	kopia_matice = malloc(sizeof(double *) * rozmer_obrazku[0]);
//	for(i = 0; i<rozmer_obrazku[0]; ++i)
//		kopia_matice[i] = malloc(sizeof(double) * rozmer_obrazku[1]);
	kopiruj_maticu(kopia_matice, matica);

	for(iteracie = 0; iteracie < 10; iteracie++){
		for(i = 0; i<pocet_bodov; ++i){
			sigma = 0.l;
			tmp_double = 0.l;
			index_to_pos(&pozicia, i);
			pos_copy(&pozicia_i, &pozicia);
			//TODO nojman
			for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; bazovy_vektor++){
				if(pozicia[bazovy_vektor] == 0){
					pozicia_i[bazovy_vektor] += 1;
				}
				else if(pozicia[bazovy_vektor] == rozmer_obrazku[bazovy_vektor] - 1){
					pozicia_i[bazovy_vektor] -= 1;
				}
				else{
					continue;
				}
				*vrat_iti_element(matica, pozicia) = 0.l;
				sigma -= 2 * cas * (*vrat_iti_element(matica, pozicia_i));
				tmp_double += 4*pocet_rozmerov*cas;
				pokracovat = 1;
				if(pozicia[bazovy_vektor] == 0){
					pozicia_i[bazovy_vektor] -= 1;
				}
				else if(pozicia[bazovy_vektor] == rozmer_obrazku[bazovy_vektor] - 1){
					pozicia_i[bazovy_vektor] += 1;
				}
			}
			if(pokracovat){
				*vrat_iti_element(matica, pozicia) = ( (*vrat_iti_element(kopia_matice, pozicia)) -sigma)/(1+tmp_double);
				pokracovat=0;
				continue;
			}

			for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor){
				pozicia_i[bazovy_vektor] += 1;
				sigma -= cas * (*vrat_iti_element(matica, pozicia_i));
				pozicia_i[bazovy_vektor] -= 2;
				sigma -= cas * (*vrat_iti_element(matica, pozicia_i));
				pozicia_i[bazovy_vektor] += 1;
				//printf("%f\n", sigma);
			}
			//printf("%f %f %f\n", sigma, *vrat_iti_element(kopia_matice, pozicia), 1+2*pocet_rozmerov*cas);
			*vrat_iti_element(matica, pozicia) = ( (*vrat_iti_element(kopia_matice, pozicia)) -sigma)/(1+2*pocet_rozmerov*cas);
		}
	}

	//vycisti_maticu(kopia_matice);
	for(i = 0; i<rozmer_obrazku[0]; ++i)
		free(kopia_matice[i]);
	free(kopia_matice);
	pos_clean(&pozicia);
	pos_clean(&pozicia_i);
}

