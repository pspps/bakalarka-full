#ifndef MATICA_H
#define MATICA_H
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <gsl/gsl_linalg.h>
#include "pos.h"
#include "malloccheck.h"

#define pocet_iteracii_gauss_seidela 800

#define alokuj_maticu(v) alokuj_maticu_implementacia(v, 0)
#define vycisti_maticu(v) vycisti_maticu_implementacia(v, 0)

double *vrat_iti_riesenia(POS pozicia);
double *vrat_iti_obrazka(POS pozicia);
double *vrat_iti_element(double **pole, POS pozicia);
double *gradient_na_hrane(double **pole, POS pozicia, int bazovy_vektor, int hore);
double velkost_gradientu_na_hrane_na_druhu(double **pole, POS pozicia, int bazovy_vektor, int hore);
//double *gradient_na_elemente(double **pole, POS pozicia);
double velkost_gradientu_na_elemente_na_druhu(double **pole, POS pozicia);
void exportuj_maticu(double **pole, char *nazov_suboru);
void exportuj_maticu_inu(double **pole, char *nazov_suboru, unsigned int velkost);
void exportuj_maticu_gaussajdel(double *diagonala, double **vedlajsie, double *prava, char *nazov_suboru);
void exportuj_maticu_normovanu(double **pole, char *nazov_suboru);
void uloz_riesenie(int cislo_riesenia);
void uloz_obrazok();
void alokuj_maticu_implementacia(double **v, int hlbka);
void vycisti_maticu_implementacia(double **v, int hlbka);
void kopiruj_maticu(double **ciel, double **zdroj);

void gauss_seidel(int dlzka, double **matica, double *ps, double *riesenie);
//double *gnu_solve(double *matica_v, double *prava_v, double *ciel, int velkost);

void vedenie_tepla(double **matica, double cas);


#endif // MATICA_H
