#include "pos.h"
#include "extern.h"



void pos_init(POS *v, double hodnota){
    size_t velkost = sizeof(double)*pocet_rozmerov;
    *v = malloc(velkost);
    int i;

    for (i = 0; i < pocet_rozmerov; ++i)
        (*v)[i] = hodnota;
}

void pos_init_viacero(unsigned int pocet, double hodnota, ...){
    unsigned int i;
    va_list argumenty;
    va_start(argumenty, hodnota);

    for(i = 0; i<pocet; ++i)
        pos_init(va_arg(argumenty, POS *), hodnota);

    va_end(argumenty);
}

void pos_copy(POS *ciel, POS *zdroj){
    int i;
    for(i = 0; i<pocet_rozmerov; ++i)
        (*ciel)[i] = (*zdroj)[i];
}

void pos_clean(POS *v){
    free(*v);
}

void pos_clean_viacero(unsigned int pocet, ...){
    unsigned int i;
    va_list argumenty;
    va_start(argumenty, pocet);

    for(i = 0; i<pocet; ++i)
        pos_clean(va_arg(argumenty, POS *));

    va_end(argumenty);
}

int pos_je_na_kraji(POS *v){
    int i;
    for(i = 0; i < pocet_rozmerov; ++i)
        if( ((*v)[i] <= 0) || ((*v)[i] >= rozmer_obrazku[i]-1) )
               return 1;

    return 0;
}
unsigned int pos_to_index(POS *v){
    int i;
    unsigned int ret = 0, koeficient_sustavy = 1;
    for(i = 0; i<pocet_rozmerov; ++i){
        ret += (*v)[i]*koeficient_sustavy;
        koeficient_sustavy *= rozmer_obrazku[i];
    }

    return ret;
}

void index_to_pos(POS *ciel, unsigned int v){
    int i;
    for(i = 0; i<pocet_rozmerov; ++i){
        (*ciel)[i] = v % rozmer_obrazku[i];
        v /= rozmer_obrazku[i];
    }
}

void pos_print(POS *v){
    int i;

    printf("{ ");
    cyklus_cez_rozmery{
        printf("%d ", (*v)[i]);
    }
    printf("}");
}
