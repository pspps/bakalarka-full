TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lgsl -lgslcblas

SOURCES += main.c \
    pos.c \
    matica.c \
#    koeficienty.c \
    init.c \
    malloccheck.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    pos.h \
    matica.h \
    koeficienty.h \
    init.h \
    extern.h \
    malloccheck.h

