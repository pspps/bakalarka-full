#ifndef EXTERN_H
#define EXTERN_H

extern const unsigned int pocet_rozmerov;
extern const double delta[];
extern const double deltaT;
extern const unsigned int rozmer_obrazku[];
extern const unsigned int pocet_iteracii;

extern double objemV;
extern double *poverchSteny;

//TODO: 2D
extern double **obrazok;
extern double **aktualne_riesenie;

extern double wcon,
       wdif,
       epsilon,
       Ka;

#endif // EXTERN_H

