#include "init.h"


void inicializuj_obrazok(){
    //TODO: 2D
    int i, j;
    const int sirka = 5;
    const int polomer = 20;

    obrazok = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


    for(int i = 0; i<rozmer_obrazku[0]; ++i){
        obrazok[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[1]; ++j){
            int x = i-(rozmer_obrazku[0]/2);
            int y = j-(rozmer_obrazku[1]/2);

            if(abs(x*x + y*y - polomer*polomer) < sirka*sirka)
                obrazok[i][j] = 10.l;
        }
    }
}

void inicializuj_riesenie(){
    int i, j;

    aktualne_riesenie = (double **)malloc(rozmer_obrazku[0] * sizeof(double *));


    for(int i = 0; i<rozmer_obrazku[0]; ++i){
        aktualne_riesenie[i] = (double *)malloc(rozmer_obrazku[1] * sizeof(double));

        for (j = 0; j < rozmer_obrazku[1]; ++j){
            int x = i-(rozmer_obrazku[0]/2);
            int y = j-(rozmer_obrazku[1]/2);

                if(abs(x*x + y*y < 10*10))
                    aktualne_riesenie[i][j] = 10.l;
        }
    }
}


void init(){
    inicializuj_obrazok();
    inicializuj_riesenie();

    int i, j;

    objemV = 1.l;
    poverchSteny = (double *)malloc(sizeof(double)*pocet_rozmerov);

    for (i = 0; i < pocet_rozmerov; ++i)
        poverchSteny[i] = 1.l;

    for (i = 0; i < pocet_rozmerov; ++i)
    {
        objemV *= delta[i];

        for (j = 0; j < pocet_rozmerov; ++j)
        {
            if(i!=j)
                poverchSteny[j] *= delta[i];
        }
    }

}
