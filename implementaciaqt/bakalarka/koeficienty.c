#include "koeficienty.h"
#include "extern.h"

double f(double v){
    return v;
}

double g(POS pozicia){
    //FIXME: gredient
    double gradient_image = sqrt(velkost_gradientu_na_elemente_na_druhu(obrazok, pozicia));
    return f(1/(1 + Ka * gradient_image * gradient_image));
}

double koeficient_prava_strana(POS pozicia){
    double riesenie, ret;
    if(pos_je_na_kraji(&pozicia))
        return 0.l;

    riesenie = *vrat_iti_riesenia(pozicia);
    ret = (objemV/deltaT) * riesenie;

    return ret;
}

double koeficient_i(POS pozicia){
    if(pos_je_na_kraji(&pozicia))
        return 1.0;

    double ret = 0.l;

    double casovy_koeficient = 0.l,
           konveksny_koeficient = 0.l,
           difuzny_koeficient = 0.l;

    casovy_koeficient = objemV/deltaT;

    int bazovy_vektor, hore;
    POS i;
    pos_init(&i, 0.l);
    pos_copy(&i, &pozicia);

    for(bazovy_vektor = 0; bazovy_vektor < pocet_rozmerov; ++bazovy_vektor)
    {
        for (hore = 0; hore < 2; ++hore)
        {
            pos_copy(&i, &pozicia);
            i[bazovy_vektor] += (hore ? 1 : -1);

            konveksny_koeficient += (poverchSteny[bazovy_vektor]/delta[bazovy_vektor]) * (g(i) - g(pozicia));
        difuzny_koeficient += poverchSteny[bazovy_vektor]/(delta[bazovy_vektor] * sqrt(epsilon*epsilon + velkost_gradientu_na_hrane_na_druhu(aktualne_riesenie, i, bazovy_vektor, hore)));

        i[bazovy_vektor] = pozicia[bazovy_vektor];
        }
    }
    pos_clean(&i);

    konveksny_koeficient *= wcon;
    difuzny_koeficient *= wdif * g(pozicia) * sqrt(epsilon*epsilon + velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia));


    ret = casovy_koeficient + konveksny_koeficient + difuzny_koeficient;

    return ret;
}

double koeficient_iplus(POS pozicia, int bazovy_vektor, int hore){
    if(pos_je_na_kraji(&pozicia))
        return 0.l;

    double ret = 0.l;

    double konveksny_koeficient = 0.l,
           difuzny_koeficient = 0.l;

    double tmp_gradient;

    POS i;
    pos_init(&i, 0.l);
    pos_copy(&i, &pozicia);
    i[bazovy_vektor] += (hore ? 1 : -1);

    konveksny_koeficient = wcon * poverchSteny[bazovy_vektor] * (g(i) - g(pozicia)) / delta[bazovy_vektor];

    tmp_gradient = velkost_gradientu_na_elemente_na_druhu(aktualne_riesenie, pozicia);
    difuzny_koeficient = -wdif * g(pozicia) * sqrt(epsilon*epsilon + tmp_gradient) * poverchSteny[bazovy_vektor] / (delta[bazovy_vektor] * sqrt(epsilon*epsilon + velkost_gradientu_na_hrane_na_druhu (aktualne_riesenie, pozicia, bazovy_vektor, hore)));

    pos_clean(&i);

    ret = konveksny_koeficient + difuzny_koeficient;

    return ret;
}
