#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "pos.h"
#include "matica.h"
#include "init.h"
#include "malloccheck.h"
#include "koeficienty.c"

#define obrazokX 5
#define obrazokY 5

const unsigned int pocet_rozmerov = 2;
const double delta[] = {1.l, 1.l};
const double deltaT = 1.l;
const unsigned int rozmer_obrazku[] = {obrazokX, obrazokY};
const unsigned int pocet_iteracii = 10;

double objemV;
double *poverchSteny;

//TODO: 2D
double **obrazok;
double **aktualne_riesenie;

double wcon = 1.l,
       wdif = 1.l,
       epsilon = .01l,
       Ka = 8.l;

int main(void)
{
    unsigned int cislo_riesenia;
    char *nazov_suboru;
    double **matica, *prava_strana, *riesenie, tmp_double;
    int i, j, k;
    POS i_pos, tmp_pos;
    int pocet_bodov = 1;

    printf("Ahoj svet\n");
    init();

    nazov_suboru = (char *)malloc(strlen("data/riesenie.00000.matica")*sizeof(char));

    exportuj_maticu(obrazok, "data/obrazok.matica");
    exportuj_maticu(aktualne_riesenie, "data/riesenie.0.matica");

    cyklus_cez_rozmery
        pocet_bodov *= rozmer_obrazku[i];
    pos_init_viacero(2, 0.l, &i_pos, &tmp_pos);

    double *matica_ptr = malloc(sizeof(double) * pocet_bodov * pocet_bodov);
    matica = (double **)malloc(sizeof(double *) * pocet_bodov);
    prava_strana = (double *)malloc(sizeof(double) * pocet_bodov);
    riesenie = (double *)malloc(sizeof(double) * pocet_bodov);

    for(i = 0; i<pocet_bodov; ++i){
        if(i>pocet_bodov)
            printf("%d", i<pocet_bodov);
        //matica[i] = (double *)malloc(sizeof(double) * pocet_bodov);
        matica[i] = matica_ptr + (i*pocet_bodov);
        for(j = 0; j < pocet_bodov; ++j)
            matica[i][j] = 0.l;
    }

    printf("Inicializacia skoncena\n");
    fflush(stdout);

    for(cislo_riesenia = 1; cislo_riesenia <= pocet_iteracii; ++cislo_riesenia){
    //for(cislo_riesenia = 1; cislo_riesenia <= 0; ++cislo_riesenia){
        printf("Zacinam %04d tu iteraciu\n", cislo_riesenia);
        fflush(stdout);
        for(i = 0; i<pocet_bodov; ++i){
            index_to_pos(&i_pos, i);

            if(pos_je_na_kraji(&i_pos)){
                matica[i][i] = 1.l;
                //TODO
                prava_strana[i] = 0.l;
                continue;
            }

            tmp_double = koeficient_i(i_pos);
            matica[i][i] = tmp_double;
            tmp_double = koeficient_prava_strana(i_pos);
            prava_strana[i] = tmp_double;
            pos_copy(&tmp_pos, &i_pos);

            for(j = 0; j < pocet_rozmerov; ++j){
                tmp_pos[j] += 1;
                matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 1);
                tmp_pos[j] -= 2;
                matica[i][pos_to_index(&tmp_pos)] = koeficient_iplus(i_pos, j, 0);
            }

        }
        printf("Matica vytvorena\n");
        fflush(stdout);
        //TODO: vymazat
        sprintf(nazov_suboru, "data/mmmmmmatica.%d.matica", cislo_riesenia);
        exportuj_maticu_inu(matica, nazov_suboru, pocet_bodov);

        memcpy(riesenie, aktualne_riesenie, sizeof(double)*pocet_bodov);
        gnu_solve(matica_ptr, prava_strana, riesenie, pocet_bodov);
        //gauss_seidel(pocet_bodov, matica, prava_strana, riesenie);

        printf("Matica vyriesena\n");
        fflush(stdout);

        for(i = 0; i<pocet_bodov; ++i){
            index_to_pos(&tmp_pos, i);
            (*vrat_iti_element(aktualne_riesenie, tmp_pos)) = riesenie[i];
        }

        sprintf(nazov_suboru, "data/riesenie.%d.matica", cislo_riesenia);
        exportuj_maticu(aktualne_riesenie, nazov_suboru);
        sprintf(nazov_suboru, "data/matica.%d.matica", cislo_riesenia);
        exportuj_maticu_inu(matica, nazov_suboru, pocet_bodov);
        printf("Koncim  %04d tu iteraciu\n", cislo_riesenia);
        fflush(stdout);
    }

    printf("Koniec :)\n");

    return 0;
}

