#include "matica.h"
#include "extern.h"

double *vrat_iti_obrazka(POS pozicia){
    return vrat_iti_element(obrazok, pozicia);
}

double *vrat_iti_riesenia(POS pozicia){
    return vrat_iti_element(aktualne_riesenie, pozicia);
}

double *vrat_iti_element(double **pole, POS pozicia){
    int i;
    double **pointer = pole;

    for(i = 0; i<pocet_rozmerov; ++i)
        if(pozicia[i] < 0 || pozicia[i] >= rozmer_obrazku[i])
            return 0;

    for (i = 0; i < pocet_rozmerov-1; ++i)
    {
        pointer = (double **)pointer[pozicia[i]];
    }

    pointer = (double **) ((double *)pointer + pozicia[pocet_rozmerov-1]);

    return (double *)pointer;
}

double *gradient_na_hrane(double **pole, POS pozicia, int bazovy_vektor, int hore){
    double *ret = (double *)malloc(sizeof(double)*pocet_rozmerov);
    int i;

    for(i=0; i<pocet_rozmerov; ++i)
        ret[i] = 0.l;

    POS pozicia_nove,
        pozicia_horne,
            pozicia_dolne;
    pos_init_viacero(3, 0.l, &pozicia_nove, &pozicia_dolne, &pozicia_horne);
    /*pos_init(&pozicia_nove, 0.l);
    pos_init(&pozicia_dolne, 0.l);
    pos_init(&pozicia_horne, 0.l);*/

    for(i=0; i<pocet_rozmerov; i++){
        if(i == bazovy_vektor){
            pos_copy(&pozicia_nove, &pozicia);
            pozicia_nove[i] += ( hore ? 1 : -1);

            ret[i] = (vrat_iti_element(pole, pozicia_nove) - vrat_iti_element(pole, pozicia)) / delta[i];
            continue;
        }
        pos_copy(&pozicia_horne, &pozicia);
        pos_copy(&pozicia_dolne, &pozicia);

        pozicia_dolne[i] += -1;
        pozicia_horne[i] += 1;

        ret[i] += vrat_iti_element(pole, pozicia_horne) - vrat_iti_element(pole, pozicia_dolne);

        pozicia_dolne[bazovy_vektor] += (hore ? 1 : -1);
        pozicia_horne[bazovy_vektor] += (hore ? 1 : -1);

        ret[i] += vrat_iti_element(pole, pozicia_horne) - vrat_iti_element(pole, pozicia_dolne);

        ret[i] /= 4*delta[i];
    }

    pos_clean_viacero(3, &pozicia_nove, &pozicia_dolne, &pozicia_horne);

    return ret;
}

double velkost_gradientu_na_hrane_na_druhu(double **pole, POS pozicia, int bazovy_vektor, int hore){
    double *tmp,
           ret = 0.l;
    int i;

    tmp = gradient_na_hrane(pole, pozicia, bazovy_vektor, hore);
    for(i=0; i<pocet_rozmerov; ++i)
        ret += tmp[i]*tmp[i];

    free(tmp);

    return ret;
}

double *gradient_na_elemente(double **pole, POS pozicia){
    double *ret = (double *)malloc(sizeof(double)*pocet_rozmerov),
           *tmp_double;
    int i, j;

    for(i=0; i<pocet_rozmerov; ++i)
        ret[i] = 0.l;

    for(i=0; i<2*pocet_rozmerov; ++i){
        tmp_double = gradient_na_hrane(pole, pozicia, i>>1, i&1);

        for(j=0; j<pocet_rozmerov; ++j)
            ret[j] += tmp_double[j];

        free(tmp_double);
    }

    for(j=0; j<pocet_rozmerov; ++j)
        ret[j] /= 2*pocet_rozmerov;

    return ret;
}

double velkost_gradientu_na_elemente_na_druhu(double **pole, POS pozicia){
    double *tmp,
           ret = 0.l;
    int i;

    tmp = gradient_na_elemente(pole, pozicia);
    for(i=0; i<pocet_rozmerov; ++i)
        ret += tmp[i]*tmp[i];

    free(tmp);

    return ret;
}

void exportuj_maticu(double **pole, char *nazov_suboru){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
    for(i = 0; i < rozmer_obrazku[0]; ++i){
        for(j = 0; j < rozmer_obrazku[1]; ++j){
            fprintf(f, "%lf ", pole[i][j]);
        }
        putc('\n', f);
    }
}

void exportuj_maticu_inu(double **pole, char *nazov_suboru, unsigned int velkost){
    //TODO 2D
    FILE *f = fopen(nazov_suboru, "w");
    if(!f){
        perror("Neviem otvorit subor\n");
        return;
    }
    int i, j;
    for(i = 0; i < velkost; ++i){
        for(j = 0; j < velkost; ++j){
            fprintf(f, "%lf ", pole[i][j]);
        }
        putc('\n', f);
    }
}

void alokuj_maticu_implementacia(double **v, int hlbka){
    int i;

    if(hlbka == pocet_rozmerov-1){
        *v = (double *)malloc(sizeof(double) * rozmer_obrazku[pocet_rozmerov-1]);

        for(i = 0; i<rozmer_obrazku[hlbka]; ++i)
            (*v)[i] = 0.l;

        return;
    }

    *v = (double *)malloc(sizeof(double *) * rozmer_obrazku[hlbka]);

    for(i = 0; i<rozmer_obrazku[hlbka]; ++i){
        alokuj_maticu_implementacia(&v[i], hlbka+1);
    }
}

void gauss_seidel(int dlzka, double **matica_v, double *ps, double *riesenie){
    int i, j, iteracie;
    double sigma;

    for(iteracie = 0; iteracie < pocet_iteracii_gauss_seidela; ++iteracie){
        for(i = 0; i<dlzka; ++i){
            sigma = 0.l;

            for(j = 0; j<dlzka; ++j){
                if(i!=j){
                    sigma += matica_v[i][j] * riesenie[j];
                }
            }

            riesenie[i] = (ps[i] - sigma) / matica_v[i][i];
        }
        printf("%f %f %f %f %f\n", riesenie[0], riesenie[1], riesenie[2], riesenie[3], riesenie[4]);
    }
}

double *gnu_solve(double *matica_v, double *prava_v, double *ciel, int velkost){
    gsl_matrix_view m
      = gsl_matrix_view_array (matica_v, velkost, velkost);

    gsl_vector_view b
      = gsl_vector_view_array (prava_v, velkost);

    gsl_vector *x = gsl_vector_alloc (velkost);

    int s;

    gsl_permutation * p = gsl_permutation_alloc (velkost);

    gsl_linalg_LU_decomp (&m.matrix, p, &s);

    gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

    memcpy(ciel, gsl_vector_ptr(x, 0), sizeof(double) * velkost);

    gsl_permutation_free (p);
    gsl_vector_free (x);
}

void vedenie_tepla(double **matica, double cas, int velkost){

}

