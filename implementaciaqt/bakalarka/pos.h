#ifndef POS_H
#define POS_H
#include <stdarg.h>
#include <stdlib.h>
#include "malloccheck.h"


typedef unsigned int* POS;
#define cyklus_cez_rozmery for(i = 0; i<pocet_rozmerov; ++i)



void pos_init(POS *v, double hodnota);

void pos_init_viacero(unsigned int pocet, double hodnota, ...);

void pos_copy(POS *ciel, POS *zdroj);

void pos_clean(POS *v);

void pos_clean_viacero(unsigned int pocet, ...);

int pos_je_na_kraji(POS *v);

unsigned int pos_to_index(POS *v);

void index_to_pos(POS *ciel, unsigned int v);

void pos_print(POS *v);



#endif // POS_H
